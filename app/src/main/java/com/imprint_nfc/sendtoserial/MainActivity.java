package com.imprint_nfc.sendtoserial;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.*;



public class MainActivity extends AppCompatActivity {
    TextView error;
    private Button mButtonStartRepeatedTask;
    private Button mButtonStopRepeatedTask;
    private Handler mHandler;
    private Runnable mRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButtonStartRepeatedTask = (Button) findViewById(R.id.start_repeat_intervals);
        mButtonStopRepeatedTask = (Button) findViewById(R.id.stop_repeat_intervals);
        final EditText mEditText = (EditText) findViewById(R.id.time_interval);
        error = (TextView) findViewById(R.id.error);

        mHandler = new Handler();

        // Листенер для кнопки запуска повторяющихся операций
        mButtonStartRepeatedTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int time_interval = 1000 * Integer.parseInt(mEditText.getText().toString());

                // Инициализируем новый Runnable
                mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        TextView main_text = (TextView) findViewById(R.id.textView);
                        try {
                            String sharedFact = main_text.getText().toString();
                            Process process = Runtime.getRuntime().exec("sh"); //даем привелегии и создаем процесс
                            OutputStream os = process.getOutputStream(); //получаем поток процесса
                            DataOutputStream dos = new DataOutputStream(os); //создаем объект класса потока
                            String cmd = "echo '" + sharedFact + "\r\n'> /dev/ttyS0";
//                            error.setText(dos.toString());
                            dos.writeBytes(cmd); //пишем байты
                            dos.writeBytes("echo '" + new String(new byte[]{(byte) 29, (byte) 86, (byte) 48}) + "\r\n'> /dev/ttyS0");
                            dos.flush(); //проталкиваем байты
                            dos.close();
                        } catch (Exception e) {
                            e.printStackTrace();
//                            error.setText(e.toString());
                        }

                        // Повторять с определенным интервалом
                        mHandler.postDelayed(this, time_interval);
                    }
                };
                // Выполнить задачу через время
                mHandler.postDelayed(mRunnable, 1000);
            }
        });

        // Листенер для остановки повторяющейся операции
        mButtonStopRepeatedTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mHandler.removeCallbacks(mRunnable);
            }
        });
    }

    public void send_command(View view) {
        TextView main_text = (TextView) findViewById(R.id.textView);
        try {
            String sharedFact = main_text.getText().toString();
            Process process = Runtime.getRuntime().exec("sh"); //даем привелегии и создаем процесс
            OutputStream os = process.getOutputStream(); //получаем поток процесса
            DataOutputStream dos = new DataOutputStream(os); //создаем объект класса потока
            String cmd = "echo '" + sharedFact + "\r\n'> /dev/ttyS0";
//            error.setText(dos.toString());
            dos.writeBytes(cmd); //пишем байты
            dos.flush(); //проталкиваем байты
            dos.close();
        } catch (Exception e) {
            e.printStackTrace();
//            error.setText(e.toString());
        }
    }


    public void send_command_special(View view) {
        try {
//            Process process = Runtime.getRuntime().exec("su"); //даем привелегии SU и создаем процесс
            Process process = Runtime.getRuntime().exec("sh");//создаем оболочку (по сути поток)
            OutputStream os = process.getOutputStream(); //получаем поток процесса
            DataOutputStream dos = new DataOutputStream(os); //создаем объект класса потока
//            error.setText(dos.toString());
            dos.writeBytes("echo '" + new String(new byte[]{(byte) 29, (byte) 86, (byte) 48}) + "\r\n'> /dev/ttyS0");
            dos.flush(); //проталкиваем байты
            dos.close();
        } catch (Exception e) {
            e.printStackTrace();
//            error.setText(e.toString());
        }
    }

    //Посылка бинарного файла по пути объявленном в file переменной через echo
    public void send_command_echo(View view) {
        try {
            //Read binary file
            File file = new File("/Data/local/1");
            byte[] fileData = new byte[(int) file.length()];

            try {
                DataInputStream dis = new DataInputStream(new FileInputStream(file));
                dis.readFully(fileData);
                dis.close();
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
//                error.setText("File not found! Motherfucker!");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
//                error.setText(e.toString());
            }

            //Run command shell and execture echo command
            Process proc = Runtime.getRuntime().exec("sh");
            OutputStream stream = proc.getOutputStream();
            DataOutputStream dos = new DataOutputStream(stream);
//            error.setText(dos.toString());
            dos.writeBytes("echo '" + new String(fileData) + "'> /dev/ttyS0");
            dos.flush(); //проталкиваем байты
            dos.close();
        } catch (Exception e) {
//            error.setText(e.toString());
        }
    }

    //Посылать с помощью скомпилированных сишных библиотек
    public void send_command_binary_file(View view) {
        try {
            File fin = new File("/dev/ttyS0");
            SerialPort sp = new SerialPort(fin, 115200, 0);
            OutputStream stream = sp.getOutputStream();
            InputStream i_stream = sp.getInputStream();
//            error.setText(stream.toString());
            //stream.write(new String("text").getBytes());
            stream.write(0x1B);
            stream.write(0x19);
            stream.write(0x31);
            Thread.sleep(50);
            if(i_stream.available() > 0)
            {

            }
            byte answer[] = new byte[300];
            i_stream.read(answer);
            String ans = new String(answer);
            error.setText(ans);
            stream.flush();
            stream.close();
            i_stream.close();
        } catch (Exception e) {
            e.printStackTrace();
            error.setText(e.toString());
        }
    }


    private static byte[] readFile(String file) throws IOException {
        return readFile(new File(file));
    }

    private static byte[] readFile(File file) throws IOException {
        // Open file
        RandomAccessFile f = new RandomAccessFile(file, "r");
        try {
            // Get and check length
            long longlength = f.length();
            int length = (int) longlength;
            if (length != longlength)
                throw new IOException("File size >= 2 GB");
            // Read file and return data
            byte[] data = new byte[length];
            f.readFully(data);
            return data;
        } finally {
            f.close();
        }
    }

    public void send_command_set(View view) {
        try {
            File fin = new File("/dev/ttyS0");
            SerialPort sp = new SerialPort(fin, 115200, 0);
            OutputStream stream = sp.getOutputStream();
            InputStream i_stream = sp.getInputStream();
//            error.setText(stream.toString());
            //stream.write(new String("text").getBytes());
//            File initialFile = new File("test_4EK");
//            InputStream targetStream = new FileInputStream(initialFile);


            byte arr[] = {0x1B, 0x47, 0x01, 0x0D, 0x0A, 0x1B, 0x61, 0x01, 0x0D, 0x0A, 0x8C - 256, 0xAE - 256, 0xA9 - 256, 0x20, 0x8C - 256, 0xA0 - 256, 0xA3 - 256, 0xA7 - 256, 0xA8 - 256, 0xAD - 256, 0x0D, 0x0D, 0x0A, 0x1B, 0x47, 0x00, 0x0D,
                    0x0A, 0x1B, 0x61, 0x30, 0x0D, 0x0A, 0x1B, 0x61, 0x01, 0x0D, 0x0A, 0x80 - 256, 0xA4 - 256, 0xE0 - 256, 0xA5 - 256, 0xE1 - 256, 0x20, 0xAC - 256, 0xA0 - 256, 0xA3 - 256, 0xA0 - 256, 0xA7 - 256, 0xA8 - 256, 0xAD - 256, 0xA0 - 256, 0x0D, 0x0D,
                    0x0A, 0x1B, 0x61, 0x30, 0x0D, 0x0A, 0x88 - 256, 0xAC - 256, 0xEF - 256, 0x20, 0xAA - 256, 0xA0 - 256, 0xE1 - 256, 0xE1 - 256, 0xA8 - 256, 0xE0 - 256, 0xA0 - 256, 0x0D, 0x0D, 0x0A, 0x8D - 256, 0xAE - 256, 0xAC - 256, 0xA5 - 256, 0xE0 - 256, 0x20, 0xAA - 256,
                    0xA0 - 256, 0xE1 - 256, 0xE1 - 256, 0xEB - 256, 0x0D, 0x0D, 0x0A, 0x8D - 256, 0xAE - 256, 0xAC - 256, 0xA5 - 256, 0xE0 - 256, 0x20, 0xE1 - 256, 0xAC - 256,
                    0xA5 - 256, 0xAD - 256, 0xEB - 256,  0x0D, 0x0D, 0x0A, 0x8D - 256, 0xAE - 256, 0xAC - 256, 0xA5 - 256, 0xE0 - 256, 0x20,
                    0xE7 - 256, 0xA5 - 256, 0xAA - 256, 0xA0 - 256, 0x0D, 0x0D, 0x0A, 0x0D, 0x0D, 0x0A, 0x0D, 0x0D, 0x0A, 0xCD - 256, 0xE0 - 256, 0xE7 - 256, 0xE2 - 256, 0xE0 - 256,
                    0xED - 256, 0xE8 - 256, 0xE5 - 256, 0x20, 0xEF - 256, 0xE5 - 256, 0xF0 - 256, 0xE2 - 256, 0xEE - 256,
                    0xE3 - 256, 0xEE - 256, 0x20, 0xF2 - 256, 0xEE - 256, 0xE2 - 256, 0xE0 - 256, 0xF0 - 256, 0xE0 - 256, 0x09, 0x31, 0x2E, 0x30, 0x32, 0x30, 0x33, 0x78, 0x31, 0x30, 0x30,
                    0x20, 0x3D, 0x31, 0x30, 0x32, 0x2E, 0x30,
                    0x33, 0x0D, 0x0D, 0x0A, 0xAD - 256, 0xA0 - 256, 0xE6 - 256, 0xA5 - 256, 0xAD - 256, 0xAA - 256, 0xA0 - 256, 0x09, 0x3D, 0x35, 0x30, 0x2E, 0x30, 0x30, 0x0D, 0x0D, 0x0A, 0xCD - 256,
                    0xE0 - 256, 0xE7 - 256, 0xE2 - 256, 0xE0 - 256, 0xED - 256,
                    0xE8 - 256, 0xE5 - 256, 0x20, 0xE2 - 256, 0xF2 - 256, 0xEE - 256, 0xF0 - 256, 0xEE - 256, 0xE3 - 256, 0xEE - 256, 0x20, 0xF2 - 256, 0xEE - 256, 0xE2 - 256, 0xE0 - 256, 0xF0 - 256, 0xE0 - 256,
                    0x09, 0x31, 0x78, 0x31, 0x35, 0x2E, 0x30, 0x30, 0x20, 0x3D,
                    0x31, 0x35, 0x2E, 0x30, 0x30, 0x0D, 0x0D, 0x0A, 0x0D, 0x0D, 0x0A, 0x0D, 0x0D, 0x0A, 0x1B, 0x47, 0x01, 0x0D, 0x0A, 0x88 - 256, 0x92 - 256, 0x8E - 256, 0x83 - 256, 0x8E - 256,
                    0x09, 0x3D, 0x31,
                    0x39, 0x32, 0x2E, 0x30, 0x33, 0x0D, 0x0D, 0x0A, 0x1B, 0x47, 0x00, 0x0D, 0x0A, 0x8A - 256, 0x80 - 256, 0x90 - 256, 0x92 - 256, 0x8E - 256, 0x89 - 256, 0x09, 0x3D, 0x32, 0x30, 0x30, 0x2E, 0x30, 0x33,
                    0x0D, 0x0D, 0x0A, 0x8D - 256, 0x80 - 256, 0x8B - 256, 0x88 - 256, 0x97 - 256, 0x8D - 256, 0x9B - 256, 0x8C - 256, 0x88 - 256, 0x09, 0x3D, 0x32, 0x30, 0x30, 0x2E, 0x30, 0x30, 0x0D, 0x0D, 0x0A, 0x91 - 256,
                    0x84 - 256, 0x80 - 256, 0x97 - 256,
                    0x80 - 256, 0x09, 0x3D, 0x38, 0x2E, 0x30, 0x30, 0x0D, 0x0D, 0x0A, 0x94 - 256, 0xE3 - 256, 0xE2 - 256, 0xA5 - 256, 0xE0 - 256, 0x20, 0xE7 - 256, 0xA5 - 256, 0xAA - 256, 0xA0 - 256, 0x20, 0x20, 0x0D, 0x20, 0x0D, 0x0A, 0x0D,
                    0x0D, 0x0A, 0x1B, 0x61, 0x30, 0x0D, 0x0A, 0x1B, 0x21, 0x00, 0x0D, 0x0A, 0x1D, 0x28, 0x6B, 0x64, 0x00, 0x31, 0x50, 0x30, 0x68, 0x74, 0x74, 0x70, 0x3A, 0x2F, 0x2F,
                    0x63, 0x68, 0x65, 0x63, 0x6B, 0x2E, 0x65, 0x67, 0x61, 0x69, 0x73, 0x2E, 0x72, 0x75, 0x3F, 0x69, 0x64, 0x3D, 0x63, 0x36, 0x63, 0x38, 0x32, 0x37, 0x38, 0x38, 0x2D,
                    0x37, 0x61, 0x38, 0x35, 0x2D, 0x34, 0x61, 0x38, 0x32, 0x2D, 0x61, 0x35, 0x63, 0x37, 0x2D, 0x32, 0x63, 0x63, 0x30, 0x65, 0x63, 0x33, 0x32, 0x66, 0x30, 0x36, 0x33,
                    0x26, 0x61, 0x6D, 0x70, 0x3B, 0x64, 0x74, 0x3D, 0x30, 0x37, 0x31, 0x31, 0x31, 0x36, 0x31, 0x30, 0x34, 0x32, 0x26, 0x61, 0x6D, 0x70, 0x3B, 0x63, 0x6E, 0x3D, 0x30,
                    0x32, 0x30, 0x30, 0x30, 0x30, 0x31, 0x31, 0x39, 0x38, 0x30, 0x36, 0x29, 0x0D, 0x0D, 0x0A, 0x0D, 0x0A, 0x1D, 0x56, 0x30, 0x0D, 0x0A, 0x10, 0x04, 0x01, 0x0D, 0x0A };
            stream.write(arr );

            stream.flush();
            stream.close();
            i_stream.close();
        } catch (Exception e) {
            e.printStackTrace();
            error.setText(e.toString());
        }
    }

}
